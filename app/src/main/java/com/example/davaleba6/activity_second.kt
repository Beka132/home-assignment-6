package com.example.davaleba6

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.widget.Toast
import com.example.davaleba6.databinding.ActivitySecondBinding

class activity_second : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

    }

    private fun init(){
        binding.btnOpenFirstActivity.setOnClickListener {
            openFirstActivity()
        }

        binding.include.ivExit.setOnClickListener{
            onDestroy()
        }
    }
    private fun openFirstActivity(){
        var newBeka=Profile(binding.etName.text.toString(),
            binding.etLastName.text.toString(),
            binding.etEmail.text.toString(),
            binding.etYearOfBirth.text.toString().toInt(),
            binding.etSex.text.toString())

        val i =intent
        i.putExtra("profile",newBeka)
        setResult(RESULT_OK,i)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}