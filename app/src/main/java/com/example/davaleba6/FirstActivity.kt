package com.example.davaleba6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.os.postDelayed
import com.example.davaleba6.databinding.ActivityFirstBinding
import com.example.davaleba6.databinding.ActivitySecondBinding
import java.lang.Character.toLowerCase

class FirstActivity : AppCompatActivity() {
    var beka=Profile()
    companion object{
        private const val REQUEST_CODE=11
    }
    private lateinit var binding: ActivityFirstBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityFirstBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        binding.tvName.setText(beka.firstName)
        binding.tvLastName.setText(beka.lastName)
        binding.tvEmail.setText(beka.email)
        binding.tvYearOfBirth.setText(beka.year.toString())
        binding.tvSex.setText(beka.gender)

        binding.include.ivExit.setOnClickListener {
            onDestroy()
        }

        binding.btnSecondActivity.setOnClickListener {
            openSecondActivity()
        }
    }

    private fun openSecondActivity(){
        var intent=Intent(this,activity_second::class.java)

        startActivityForResult(intent, REQUEST_CODE)
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK&&resultCode== RESULT_OK){
            var returned:Profile=data?.getParcelableExtra<Profile>("profile") as Profile
            binding.tvName.setText(returned.firstName.toString())
            binding.tvLastName.setText(returned.lastName.toString())
            binding.tvEmail.setText(returned.email.toString())
            binding.tvYearOfBirth.setText(returned.year.toString())
            binding.tvSex.setText(returned.gender.toString())
            if (binding.tvSex.text.toString().toLowerCase() =="male"){
                if (binding.tvYearOfBirth.text.toString().toInt()>2003){
                    binding.ivProfile.setImageResource(R.drawable.boy)
                }else if (binding.tvYearOfBirth.text.toString().toInt()>1970){
                binding.ivProfile.setImageResource(R.drawable.man)
                } else{
                    binding.ivProfile.setImageResource(R.drawable.old_man)
                }
            }else if(binding.tvSex.text.toString().toLowerCase()=="female"){
                if (binding.tvYearOfBirth.text.toString().toInt()>2003){
                    binding.ivProfile.setImageResource(R.drawable.girl)
                }else if (binding.tvYearOfBirth.text.toString().toInt()>1970){
                    binding.ivProfile.setImageResource(R.drawable.woman)
                } else{
                    binding.ivProfile.setImageResource(R.drawable.old_woman)
                }
            }else{
                binding.ivProfile.setImageResource(R.drawable.unisex)
            }
        }else{

        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}